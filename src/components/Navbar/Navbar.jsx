import React from 'react';

import './Navbar.scss';

class Navbar extends React.Component {
  render() {
    return (
      <nav>
        <h3>Logo</h3>

        <ul>
          <li>
            <a href="/">Home</a>
          </li>
          <li>
            <a href="/contact">Contact</a>
          </li>
          <li>
            <a href="/about">About</a>
          </li>
        </ul>
      </nav>
    );
  }
}

export default Navbar;
