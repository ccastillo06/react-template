import React from 'react';

import Navbar from './components/Navbar';

import './App.css';

// Un archivo que sea un componente, debe tener:
// * Extensión .js y el nombre en mayúsculas
// * Exportar default una función o una clase con el mismo nombre del archivo
// function App() {
//   return (
//     <div className="App">
//       <h1>Welcome to my App</h1>
//     </div>
//   );
// }

// Componente de tipo clase
class App extends React.Component {
  render() {
    return (
      <div className="App">
        <Navbar />
        <h1>Welcome to my App</h1>
      </div>
    );
  }
}

export default App;
